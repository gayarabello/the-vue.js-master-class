

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import AppDate from "@/components/AppDate";




createApp(App).component('AppDate', AppDate).use(router).mount('#app')