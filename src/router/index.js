import { createWebHistory, createRouter } from "vue-router";
import Home from "@/pages/PageHome";
import ThreadShow from "@/pages/PageThreadShow";
import Forum from "@/pages/PageForum";
import NotFound from "@/pages/NotFound";

const routes =
    [
        {
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/thread/:id',
            name: 'ThreadShow',
            component: ThreadShow,
            props: true,
        },
        {
            path: '/:pathMatch(.*)*',
            name: 'NotFound',
            component: NotFound,
        },
        {
            path: '/forum/:id',
            name: 'forum',
            component: Forum,
            props: true,
        }
    ]

const router = createRouter({
    history: createWebHistory(), routes,
});

export default router;